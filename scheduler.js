const workcentersInUse = [];
var orderSchedule = [];
var orderScheduleObj = [];
var orderFinished = false;

// main function for simulating part production. 
var producePart = function (part) {
  if (!part.done) {
    var timesToProduce = 0;
    workcentersInUse.push(part.workcenter);
    part.quantity = --part.quantity;
    if (part.quantity === 0) {
      part.done = true;
    }
    var index = workcentersInUse.indexOf(part.workcenter);
    if (index > -1) {
      workcentersInUse.splice(index, 1);
    }
  }
  return part.done;
}

module.exports = {

  schedule: function (vehicleOrder) {
    var i = 0;
    var timesToProduce = 0;
    while (!orderFinished) {
      // pick part with highest quantity left
      var part = vehicleOrder[i];
      var nextQuantity = vehicleOrder[i + 1] ? vehicleOrder[i + 1].quantity : vehicleOrder[0].quantity;
      while (part.quantity >= nextQuantity && part.quantity > 0) {
        var produced = producePart(part);
        timesToProduce++;
        if (produced && !orderSchedule.includes(part.name)) {
          orderSchedule.push(part.name);
        }
        if (orderSchedule.length === vehicleOrder.length) {
          orderFinished = true;
        }
      }
      i = vehicleOrder[i + 1] ? i + 1 : 0;
      if (timesToProduce) {
        orderScheduleObj.push({
          name: part.name,
          workcenter: part.workcenter,
          'Parts to produce': timesToProduce,
        });
      }
      timesToProduce = 0;
    }
  },

  runScheduler: function (vehicleOrder) {
    console.log('\n');
    console.log('-------------------- ORDER -----------------------');
    console.table(vehicleOrder);
    // sort order by descending quantity and workcenter
    vehicleOrder.sort((a, b) => b.quantity - a.quantity);
    vehicleOrder.sort((a, b) => a.workcenter - b.workcenter);
    this.schedule(vehicleOrder);
    console.log('\n');
    console.log('-------------------- SCHEDULE -----------------------');
    console.table(orderScheduleObj);
  }
};