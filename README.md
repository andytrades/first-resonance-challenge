# Resonant Scheduler

This script builds an optimal production schedule from a user-provided bill of materials.

The user enters part name, quantity, and workcenter via the command line and receives a schedule for part production that aims to finish production on all parts simultaneously.

I was a little unclear on whether or not workcenters could synchronously produce parts; if so, assume that scheduled part production at different workcenters happens synchronously.

### To run:

Run `npm install`.

`npm start` runs the scheduler with user input.

`npm test` runs the scheduler on editable data within the test.js file.