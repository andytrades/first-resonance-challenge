var scheduler = require('./scheduler');

var vehicleOrder = [
    {
        name: 'part one',
        quantity: 25,
        workcenter: 'A',
    },
    {
        name: 'part two',
        quantity: 50,
        workcenter: 'B',
    },
    {
        name: 'part three',
        quantity: 25,
        workcenter: 'A',
    },
    {
        name: 'part four',
        quantity: 25,
        workcenter: 'C',
    },
    {
        name: 'part five',
        quantity: 15,
        workcenter: 'B',
    },
    {
        name: 'part six',
        quantity: 10,
        workcenter: 'D',
    }
];

scheduler.runScheduler(vehicleOrder);
