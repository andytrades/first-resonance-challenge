var scheduler = require('./scheduler');
const readline = require('readline');
const art = require('ascii-art');
const Canvas = require('canvas');

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

var userOrder = [];
var userDone = false;
var firstOrNext = 'first';

var part = {};
const partName = () => {
  return new Promise((resolve, reject) => {
    rl.question('What is the name of the ' + firstOrNext + ' part you want produced? ', (name) => {
      part.name = name;
      firstOrNext = 'next';
      resolve()
    })
  })
}
const quantity = () => {
  return new Promise((resolve, reject) => {
    rl.question('What quantity do you want produced? ', (quantity) => {
      quantity = parseInt(quantity, 10); // make sure we're getting an integer value
      if (!quantity) {
        rl.write('Please enter an integer value. \n');
        process.exit();
      } else {
        part.quantity = quantity;
      }
      resolve()
    })
  })
}
const workcenter = () => {
  return new Promise((resolve, reject) => {
    rl.question('At what workcenter is this part produced? ', (workcenter) => {
      part.workcenter = workcenter;
      userOrder.push(part);
      resolve()
    })
  })
}
const another = () => {
  return new Promise((resolve, reject) => {
    rl.question('Would you like to add another part to your order? (y/n) ', (answer) => {
      if (answer != 'y') {
        rl.write('\nThanks, building your schedule now!')
        userDone = true;
      } else {
        rl.write('\n');
        part = {};
      }
      resolve()
    })
  })
}

function renderIntro() {
  var image = new art.Image({
    filepath: './saturn.jpg',
    alphabet: 'variant1'
  });

  return new Promise(resolve => {
    image.write((err, rendered) => {
      console.log(rendered);
      console.log('-------------------- RESONANT SCHEDULER -----------------------\n')
      console.log("First, we'll ask a few questions to build your order.\n");
      resolve();
    });
  })
}

const main = async () => {
  await renderIntro();
  while (!userDone) {
    await partName();
    await quantity();
    await workcenter();
    await another();
  }
  rl.close();
  scheduler.runScheduler(userOrder);
}

main();